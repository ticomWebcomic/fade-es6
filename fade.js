class Fade {
  fadeIn(el) {
    el.classList.add("show");
    el.classList.remove("hide");
  }

  fadeOut(el) {
    el.classList.add("hide");
    el.classList.remove("show");
  }

  /**
   * Inicializa la interfaz, asignando las funciones para fadeIn y fadeOut
   * Asigna el fade al listener click del boton correspondiente
   */
  inicializador() {
    var btn = document.getElementById("boton");
    var off = document.getElementById("off");
    var fade = new Fade();

    btn.addEventListener("click", function() {
      var img = document.getElementById("imagen");
      fade.fadeIn(img);
    });

    off.addEventListener("click", function() {
      var img = document.getElementById("imagen");
      fade.fadeOut(img);
    });
  }
}

export { Fade };
